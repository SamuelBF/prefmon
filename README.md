# Présentation du projet

Prefmon est un logiciel de surveillance des délais de rendez-vous pour les
démarches administratives en préfecture : il visite les sites des préfectures
lorsqu'on lui demande puis enregistre les délais dans une base de données.

# Code transféré

Nouvelle URL : [https://framagit.org/lacimade/prefmon](https://framagit.org/lacimade/prefmon)

Le code a été transféré à [la Cimade](https://www.lacimade.org) et est maintenant hébergé sur
framagit.

C'est à cette nouvelle adresse que vous trouverez le code et les informations de suivi.
